const express =require('express')
const usersController = require('./users.controller')

const userRouter = express.Router()
// USER
// getall users
userRouter.get('/',usersController.getAll)
// post
userRouter.post('/signup', usersController.signup)
// login
userRouter.post('/login',usersController.userslogin)
// delete
userRouter.delete('/:id',usersController.deleteRecord)



// user BIO

// getbyid 
userRouter.get('/details/:id',usersController.getById)




// update
userRouter.put('/details/:id',usersController.updateUser)


// history game

// get historygame
userRouter.get('/game/',usersController.getAllGame)
// get history game by user_id
userRouter.get('/game/:id',usersController.getGameById)
//post history game
userRouter.post('/game/',usersController.postGame)



module.exports =  userRouter;