
const { where } = require('sequelize');
const db = require('../db/models/');
const md5 = require('md5');
const e = require('express');



class usersModel{
    // get all users data
    getAllUsers = async() =>{
        const users = await db.UserGames.findAll({include: [db.user_biodata,db.user_game_history]})
        return users;
      }
    // getallgame
    getAllGame = async() =>{
        const game = await db.user_game_history.findAll()
        return game
    }
    getGameByUserId = async(user_id) =>{
        const user = await db.user_game_history.findOne({
            where :{user_id:user_id},
        })
        return user;

    }
    // pushgame
    pushGame = async({game_name,result,user_id}) =>{
        const pushUser= await db.user_game_history.create({
            game_name:game_name,
            result: result,
            user_id:user_id 
               })
        return pushUser
    }
    // get user data by user_id
    getUserById = async(id) =>{
        const user = await db.user_biodata.findOne({
            where :{user_id:id},
        })
        return user;
    }
   
    //   data is exist or not
    dataIsExist = async(data) =>{
        const exist = await  db.UserGames.findOne({
            where :{username: data.username},
        })
         
        if (exist) {
            return true;
            
        } else {
            return false
            
        }

    }   
   

    // record Data
    pushData = async(data) =>{
      const pushUser= await db.UserGames.create({
    
                        username:data.username,
                        email: data.email,
                        // encode password with md5
                        password: md5(data.password),
                        
                      
                    });
        return pushUser;

    }

    //login verification 
    verifiyLogin = async(username,password) =>{
        const findData = await  db.UserGames.findOne({
            where :{username:username, password:md5(password)}
            })
            // console.log(user.username)
            //     return user.username === username 
            
            //         user.password === md5(password)
         
        return findData;

    }
    dataIsRecorded= async(id) => {
        const exist = await  db.user_biodata.findOne({
            where :{user_id: id},
        })
        return exist
            


       
         
        // if (exist) {
        //     return true;
            
        // } //else {
        //     console.log(exist)
        //     return this.insertUser()
            
        // }
    //     const user = await db.user_biodata.findOne({
    //         where :{user_id:id},
    //     })
    //     return user;
    // }
    }
    // insert to user_Biodata
    insertUser = async(data) => {
        console.log(data);
        

        
        const rowsInserted = await db.user_biodata.create({
            firstname:data.firstname,
            lastname:data.lastname,
            fullname:data.fullname,
            address:data.address,
            user_id:data.user_id
        },{where: {user_id:data.user_id}});
        return rowsInserted;
    }
    // update user
    updateUser = async({firstname,lastname,fullname,address,id}) => {
        const rowsUpdated = await db.user_biodata.update(
            {fullname,
             firstname,
             lastname,
             address,
            },{where:{user_id:id}}

        );
        console.log(id,fullname)
        return rowsUpdated


    }
    // delete user::
    deleteUser = async(id) => {
        const rowsDeleted = await db.UserGames.destroy({
            where:{
                id: id
            }
        });
        console.log(rowsDeleted)
        return rowsDeleted;
    }


}
module.exports =  new usersModel();
