'use strict';
const {
  Model
} = require('sequelize');
const user_biodata = require('./user_biodata');
module.exports = (sequelize, DataTypes) => {
  class UserGames extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserGames.hasOne(models.user_biodata,{foreignKey: "user_id"})
      UserGames.hasMany(models.user_game_history,{foreignKey: "user_id"})
    }
  }
  UserGames.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UserGames',
  });
  return UserGames;
};