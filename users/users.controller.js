const usersModel = require('./users.model')


class usersController{
    // user API endpoint
    getAll = async(req,res) =>{
        const getAll = await usersModel.getAllUsers()
        return res.json(getAll)

    }
   
    signup = async(req,res) =>{
        const data = req.body
        // data validation before post to users 
        if (data.username  == "" || data.username === undefined) {
           return  res.status(404).json({ message: "username not found"})
            
        }if (data.email === '' || data.email === undefined) {
            
           return  res.status(404).json({ message: "email not found"})
    
        }if (data.password === "" || data.password === undefined){
           return res.status(404).json({ message: "password not found"})
            
          
        }
        const IsExist = await usersModel.dataIsExist(data)
        console.log(IsExist)
    
        if (IsExist) {
          
           return res.json({message: "Username already exists"})
            
        }
            usersModel.pushData(data);
            // status success code and response while post to users success
           return res.status(200).json({ message: "Success to insert new users" });  
    }
     // login
     userslogin = async(req,res) =>{
        const {username,password} = req.body;
          
        const findData =  await usersModel.verifiyLogin(username,password)
         
        if (findData) {
           return res.status(200).json({message: 'creadential data found'})
            
        } else {
            return res.status(404).json({message:"Credential not found"})
            
        }
    
    

    }
    // delete

    deleteRecord = async (req, res) => {
        
        try {
            const {id} = req.params
            
            const rowsDeleted =  await usersModel.deleteUser(id);
            console.log(rowsDeleted)
         
            if (rowsDeleted === 0) {
            return res.status(404).json({ message: 'Record not found' });
            }else{
    
                return res.status(200).json({ message: 'Record deleted' });
            }
        } catch (error) {
            console.error('Error deleting record:', error);
            res.status(500).json({ message: 'Internal server error' });
        }
    }

    // user biodata

    // get users bio by id
    getById = async(req,res) =>{
        const {id} = req.params
        const getid = await usersModel.getUserById(id)
        console.log(id,getid)
        if (getid) {
            return res.status(200).json(getid)
        } else {
            return res.status(404).json({ message: `${id} in user biodata not found` });
        }
    }
   
   
    // update
    updateUser = async(req,res) =>{
        
        
        // console.log(fullname,id,lastname)
        
       const {id} = req.params
        const recorded = await usersModel.getUserById(id)
        // const recordedUser = await usersModel.pushData(id)
        // console.log(recordedUser)
        if(recorded !== null  ){
            const {id} = req.params
            const {firstname,lastname,fullname,address}= req.body
            const updateData = await usersModel.updateUser({firstname,lastname,fullname,address,id})
            console.log(fullname,firstname,id)
            console.log(usersModel.getUserById(id))
           
            console.log(updateData)
            return res.status(200).json({message:`user id ${id} updated`})     
           
        }else  {
          

            const {firstname,lastname,fullname,address,user_id}= req.body
            console.log(user_id)
        
            const insertData = await usersModel.insertUser({firstname,lastname,fullname,address,user_id});
            console.log(`data inserted`)
            return res.status(200).json(insertData);
           
           
        

        
    



            
            // if (insertData) {
            //   
               
                
            // } else {
            //     res.status(404).json({message:`data with ${id} not inserted` })
                
            // }
           

          
        }
       
    }
    


    // game history API
    getAllGame = async(req,res)=>{
        try {
            const getAll = await usersModel.getAllGame()
            return  res.json(getAll)
         
        } catch (error) {
            return res.json(error)
        }
    }
    getGameById = async(req,res) =>{
        try {
            const {id} = req.params
            const getuserid = await usersModel.getGameByUserId(id)
            if (getuserid) {
                return res.status(200).json(getuserid)
            } else {
                return res.status(404).json({ message: ` ${id} not  found` });
            }

            
        } catch (error) {
            
        }
        res.json({message:"success to get history game by user id "})
    }
    // postGame
    postGame = async(req,res) =>{
        const data = req.body
        const pushdata = await usersModel.pushGame(data)
        if (pushdata) {
            return res.status(200).json({message:"success to inserted data game"})
        } else {
            return res.status(404).json({ message: 'Record game not found' });
        }
    }
    
   
}

module.exports = new usersController();
